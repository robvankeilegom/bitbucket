# Changelog

All notable changes to `bitbucket` will be documented in this file

## 2.0.0
- Add support for multiple accounts
- Add functionality to disable repos during sync

## 1.0.0
- Inital release
