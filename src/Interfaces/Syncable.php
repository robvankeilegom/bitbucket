<?php

namespace RoobieBoobieee\Bitbucket\Interfaces;

interface Syncable{
  public static function primaryKey();
}
