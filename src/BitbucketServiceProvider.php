<?php

namespace RoobieBoobieee\Bitbucket;

use Illuminate\Support\ServiceProvider;

class BitbucketServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
      $this->loadMigrationsFrom(__DIR__.'/Database/Migrations');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
      //
    }
}
