<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsernameToBitbucketRepositoriesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('bitbucket_repositories', function (Blueprint $table) {
      $table->string('username')->nullable()->default(null);
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('bitbucket_repositories', function (Blueprint $table) {
      $table->dropColumn('username');
    });
  }
}
