<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitbucketCommitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitbucket_commits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash');
            $table->string('repository_uuid');
            $table->string('author_user_uuid')->nullable()->default(null);
            $table->datetime('bb_date');
            $table->longText('message');
            $table->string('checksum', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitbucket_commits');
    }
}
