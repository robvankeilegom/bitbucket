<?php

namespace RoobieBoobieee\Bitbucket\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use RoobieBoobieee\Bitbucket\Jobs\SyncData;
use RoobieBoobieee\Bitbucket\Models\Commit;
use RoobieBoobieee\Bitbucket\Models\Repository;

class SyncBitbucket implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $username;
    private $password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($username, $password)
    {
      $this->username = $username;
      $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      SyncData::dispatch($this->username, $this->password, Repository::class, 'repositories?role=member', true);

      $repos = Repository::where('sync', true)->where('username', $this->username)->get();
      foreach($repos as $repo) {
        SyncData::dispatch($this->username, $this->password, Commit::class, "repositories/$repo->full_name/commits");
      }
    }
}
