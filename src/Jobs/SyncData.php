<?php

namespace RoobieBoobieee\Bitbucket\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use RoobieBoobieee\Bitbucket\Interfaces\Syncable;
use RoobieBoobieee\Bitbucket\Bitbucket;

class SyncData implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable;

  private $type;
  private $url;

  private $auto;
  private $force;

  private $username;
  private $password;

  /**
  * Create a new job instance.
  *
  * @return void
  */
  public function __construct($username, $password, $type, $url, $force = false, $auto = true)
  {
    $this->username = $username;
    $this->password = $password;

    $this->type = (new $type);
    $this->url = $url;
    $this->auto = $auto;
    $this->force = $force;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle()
  {
    $bb = new Bitbucket($this->username, $this->password);
    $data = $bb->call($this->url);
    $fillable = $this->type->getFillable();
    $pk = $this->type->primaryKey();

    if (!$data || !$data->values) {
      \Log::error(\json_encode($data));
      return;
    }

    foreach ($data->values as $record) {
      $checksum = md5(json_encode($record));
      $obj = $this->type::where($pk, $record->{$pk})->first();

      if ($obj && !$this->force) {
        return;
      }

      $record = $this->flattenObjectToArray($record);
      $record = array_filter($record, function($key) use ($fillable) {
        return in_array($key, $fillable);
      }, ARRAY_FILTER_USE_KEY);

      if (!$obj || $obj->checksum !== $checksum) {
        if (!$obj) {
          $obj = new $this->type((array) $record);

          if ($bb->uuid() === $obj->owner_uuid) {
            $obj->sync = true;
          }
        } else if ($obj->checksum !== $checksum) {
          $obj->update((array) $record);
        }

        $obj->checksum = $checksum;
        if (method_exists($obj, 'setUserName')) {
          $obj->setUserName($this->username);
        }
        $obj->save();
      }
    }

    if (property_exists($data, 'next')) {
      if (method_exists($obj, 'saveLastPage')) {
        $obj->saveLastPage($data->next);
      }
      if ($this->auto) {
        SyncData::dispatch($this->username, $this->password, get_class($this->type), $data->next, $this->auto);
      }
    }
  }

  public function flattenObjectToArray($object, $prefix = '') {
    $out = [];
    foreach($object as $key => $value) {
      if ($prefix) {
        $key = implode('_', [$prefix, $key]);
      }

      if (is_object($value) || is_array($value)) {
        $value = $this->flattenObjectToArray($value, $key);
      }

      if ($key === 'created_on' || $key === 'updated_on' || $key === 'date') {
        $key = 'bb_' . $key;
        $value = new \Carbon\Carbon($value);
      }

      if (is_array($value)) {
        $out = array_merge($out, $value);
      } else {
        $out[$key] = $value;
      }
    }
    return $out;
  }
}
