<?php

namespace RoobieBoobieee\Bitbucket\Models;

use Illuminate\Database\Eloquent\Model;
use RoobieBoobieee\Bitbucket\Interfaces\Syncable;

class Repository extends Model implements Syncable
{
  protected $table = 'bitbucket_repositories';

  protected $fillable = [
    'id',
    'scm',
    'website',
    'has_wiki',
    'uuid',
    'fork_policy',
    'name',
    'project_key',
    'project_type',
    'project_uuid',
    'project_name',
    'language',
    'bb_created_on',
    'full_name',
    'has_issues',
    'bb_updated_on',
    'size',
    'type',
    'slug',
    'is_private',
    'description',
    'owner_uuid',
  ];

  protected $appends = ['last_commits_page'];

  public static function primaryKey()
  {
    return 'uuid';
  }

  public function getLastCommitsPageAttribute($value)
  {
    return $value ?: "repositories/$this->full_name/commits";
  }

  public function setUserName($username)
  {
    return $this->username = $username;
  }
}
