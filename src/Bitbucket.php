<?php

namespace RoobieBoobieee\Bitbucket;

use GuzzleHttp\Client;
use RoobieBoobieee\Bitbucket\Models\Commit;

class Bitbucket
{

  private $username;
  private $password;

  private $user;

  private $client;

  public function __construct($username, $password)
  {
    $this->client = new Client([
      'base_uri' => 'https://api.bitbucket.org/2.0/'
    ]);
    $this->username = $username;
    $this->password = $password;
    
    $this->user = $this->call('user');
  }

  public function call($endpoint)
  {
    $options = [
      'auth' => [$this->username, $this->password]
    ];

    $response = $this->client->request('get', $endpoint, $options);

    return json_decode($response->getBody());
  }

  public function username()
  {
    return $this->user->username;
  }

  public function uuid()
  {
    return $this->user->uuid;
  }

  public function commits()
  {
    return Commit::where('author_user_uuid', $this->uuid());
  }
}
